#coding:utf8
u'''
given input array nums=[3,2,2,3],val=3

your function should return length=2,with the first two elements being 2
'''
def remove_element(nums,n):
	index = 0
	for i in range(0,len(nums)):
		if nums[i] != n:
			nums[index] = nums[i]
			index += 1
	return index

if __name__ == '__main__':
    # nums = [1,2,3,4,5,6,7,8,8,9,0,0]
    nums = [2,2,2,3]
    res = remove_element(nums,2)
    print "结果是", res,nums
