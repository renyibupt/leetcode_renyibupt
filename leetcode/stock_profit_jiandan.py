#coding:utf8
u'''
python实现股票最大收益问题
一行代码解决问题
'''
import random

def stock_profit(stocklist):
	# stocklist = []
	# for i in range(10):
	# 	stocklist.append(random.randint(1,100))

	return sum(max(stocklist[i+1]-stocklist[i], 0) for i in range (len(stocklist)-1))

if __name__ == '__main__':
	stocklist = [97, 53, 60, 13, 97, 53, 74, 28, 43, 58]
	res = stock_profit(stocklist)
	print "结果是", res
