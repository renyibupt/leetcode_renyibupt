# coding:utf8
u'''
given input array nums=[3,2,2,3],val=3

your function should return length=2,with the first two elements being 2
'''


def remove_element_from_array(nums, target):
    index = 0
    for i in range(len(nums)):
        if nums[i] != target:
            nums[index] = nums[i]
            index += 1
    return index


if __name__ == '__main__':
    nums = [3,2,2,3]
    res = remove_element_from_array(nums,2)
    print res
    print nums



