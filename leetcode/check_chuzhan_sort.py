#coding:utf8
u'''
检查出栈序列是否合法
对于入栈顺序1，2，3，4，5
		  4，3，2，1，5 就是一个合法的出栈序列
		  4，2，3，1，5 就不合法
'''
def check_chuzhan_sort(ruzhan,chuzhan):
	stack = []
	j = 0
	for i in range(len(ruzhan)):
		stack.append(ruzhan[i])
		while j < len(chuzhan) and stack[-1] == chuzhan[j]:
			stack.pop()
			j += 1
	if len(stack) == 0:
		return True
	else:
		return false
