# coding:utf8


class node(object):
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


def lianbiao_reverse(head):
    if not head:
        return
    l, m, r = None, None, head
    while r.next:
        l = m
        m = r
        r = r.next
        m.next = l
    r.next = m
    return r


if __name__ == '__main__':
    l1 = node(4)
    l1.next = node(6)
    l1.next.next = node(10)
    l1.next.next.next = node(9)
    new_head = lianbiao_reverse(l1)
    print (new_head.value,new_head.next.value,new_head.next.next.value,new_head.next.next.next.value)


