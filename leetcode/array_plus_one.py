# coding:utf8
u'''
题目：
给定一个非负整数组成的非空数组，在该数的基础上加一，返回一个新的数组。
最高位数字存放在数组的首位， 数组中每个元素只存储一个数字。
你可以假设除了整数 0 之外，这个整数不会以零开头。

示例 1:
输入: [1,2,3]
输出: [1,2,4]
解释: 输入数组表示数字 123。

示例 2:
输入: [4,3,2,1]
输出: [4,3,2,2]
解释: 输入数组表示数字 4321。
'''


def array_plus_one(nums):
    sum = 0
    for i in nums:
        sum = sum * 10 + i

    return [int(x) for x in str(sum+1)]


if __name__ == '__main__':
    nums = [8,3,5]
    res = array_plus_one(nums)
    print res





