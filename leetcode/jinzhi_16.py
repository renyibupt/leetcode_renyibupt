#coding:utf8
u'''
用户给定一个十进制数据，以及进制n， 完成n进制的转换，输出结果给用户（不要借助已有函数）
包含16进制
'''


def jinzhi(num, n):
	shiliu = [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f']
	temp = []
	result = []
	while True:
		shang = num // n #商
		yushu = num % n  #余数
		temp.append(yushu)
		if shang == 0:
			break
		num = shang
	temp.reverse()
	for i in temp:
		result.append(shiliu[i])
	result = ''.join(map(str,result))
	return result

if __name__ == '__main__':
	num = int(raw_input("输入一个数:"))
	n = int(raw_input("输入要转的进制:"))
	result = jinzhi(num,n)
	print "结果是", result

