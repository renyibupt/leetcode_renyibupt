#coding:utf8

def solution(nums):
    index = 0
    for i in range(1,len(nums)):
        if nums[index] != nums[i]:
            index += 1
            nums[index] = nums[i]
    return index+1

if __name__ == '__main__':
    nums = [1,2,3,4,4,3]
    res = solution(nums)
    print "结果是", res,nums