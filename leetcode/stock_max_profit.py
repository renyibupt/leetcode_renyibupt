#coding:utf8
u'''
python实现股票最大收益问题
'''
import random

def stock_max_profit():
	stocklist = []
	for i in range(10):
		stocklist.append(random.randint(1,100))
	print "初始股票行情列表",stocklist

	newlist = []
	for i in range(len(stocklist)):
		if i < len(stocklist) - 1:
			sub = stocklist[i+1] - stocklist[i]
			newlist.append(sub)
	print "股票日差值列表", newlist

	sumlist = [i for i in newlist if i > 0]
	print "股票正收益列表", sumlist

	return sum(sumlist)

if __name__ == '__main__':
	res = stock_max_profit()
	print "股票最大收益结果为", res