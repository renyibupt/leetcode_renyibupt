# coding:utf8
u'''
nums = [2, 2, 2, 2, 3]
return nums = [2, 3, 2, 2, 2]
'''


def remove_duplicate(nums):
    index = 0
    for i in range(1, len(nums)):
        if nums[i] != nums[index]:
            index += 1
            nums[index] = nums[i]
    return index+1


if __name__ == '__main__':
    nums = [1, 2, 3, 4, 4, 3]
    res = remove_duplicate(nums)
    print res
    print nums



