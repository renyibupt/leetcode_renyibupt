#coding:utf8
u'''
用python实现反转一个链表
'''
class Node(object):
	def __init__(self,value,next=None):
		self.value = value
		self.next = next

def reverse_lianbiao(head):
	if head == None:
		return
	L,M,R = None,None,head
	while R.next != None:
		L = M
		M = R
		R = R.next
		M.next = L
	R.next = M
	return R

if __name__ == '__main__':
	l1 = Node(1)
	l1.next = Node(2)
	l1.next.next = Node(3)
	l1.next.next.next = Node(45)
	new_head = reverse_lianbiao(l1)
	print (new_head.value,new_head.next.value,new_head.next.next.value,new_head.next.next.next.value)
