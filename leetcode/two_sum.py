#coding:utf8
u'''
英文描述：Given an array of integers, return indices of the two numbers such that they add up to a specific

target.You may assume that each input would have exactly one solution, and you may not use the same

element twice.

Given nums = [2, 7, 11, 15], target = 9,
    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1]
'''
class solution(object):
	def twoSum(self,nums,target):
		res_dict = {}
		for index,value in enumerate(nums):
			sub = target - value
			if sub in res_dict:
				return [res_dict[sub], index]
			else:
				res_dict[value] = index

if __name__ == '__main__':
	func = solution()
	nums = [2, 7, 1, 8, 17, 45, 34]
	res = func.twoSum(nums,9)
	print res