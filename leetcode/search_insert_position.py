#coding:utf8
u'''
given a sorted arrat and a target value
[1,3,5,6] 5->2
[1,3,5,6] 7->4
[1,3,5,6] 2->1
[1,3,5,6] 0->0
'''
def search_insert_position(nums,target):
	left = 0
	right = len(nums) - 1
	while left <= right:
		mid = (left + right) // 2
		if nums[mid] == target:
			return mid
		elif nums[mid] > target:
			right = mid - 1
		else:
			left = mid + 1
	return left

if __name__ == '__main__':
	nums = [1,3,5,6]
	res = search_insert_position(nums,7)
	print "结果是", res