# coding:utf8


def quick_sort(nums, low, high):
    if low >= high:
        return
    i = low
    j = high
    base = nums[i]
    while i < j:
        while i < j and nums[j] >= base:
            j -= 1
        nums[i], nums[j] = nums[j], nums[i]

        while i < j and nums[i] <= base:
            i += 1
        nums[i], nums[j] = nums[j], nums[i]

    quick_sort(nums, i+1, high)
    quick_sort(nums, low, i-1)


def bubble_sort(nums):
    for i in range(len(nums)-1):
        for j in range(len(nums)-i-1):
            if nums[j] > nums[j+1]:
                nums[j], nums[j+1] = nums[j+1], nums[j]


if __name__ == '__main__':
    nums = [4, 7, 9, 3, 1, 2, 5, 8, 6]
    quick_sort(nums, 0, len(nums)-1)
    print nums

    nums = [4, 7, 9, 3, 1, 2, 5, 8, 6]
    bubble_sort(nums)
    print nums


