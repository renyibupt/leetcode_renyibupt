# coding:utf8


def reverse_jinzhi(num, n):
    result = ''
    if num:
        result = reverse_jinzhi(num // n, n)
        return result + str(num % n)
    else:
        return result
