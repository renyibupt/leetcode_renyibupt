# coding:utf8
u'''
given a sorted array and a target value
[1,3,5,6] 5->2
[1,3,5,6] 7->4
[1,3,5,6] 2->1
[1,3,5,6] 0->0
'''


def find_insert_position(nums, target):
    low = 0
    high = len(nums) - 1
    while low <= high:
        mid = (low + high) / 2
        if nums[mid] == target:
            return mid
        elif nums[mid] >= target:
            high = mid - 1
        else:
            low = mid + 1
    return low


if __name__ == '__main__':
    nums = [1, 3, 5, 6]
    res = find_insert_position(nums, 0)
    print res







