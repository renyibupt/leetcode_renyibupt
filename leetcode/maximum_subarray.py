#coding:utf8
u'''
given the array [-2,1,-3,4,-1,2,1,-5,4]

子列 [4,-1,2,1] has the largest sum = 6
'''
def maximum_subarray(nums):
	maxsum = cursum = nums[0]
	for i in range(1,len(nums)):
		cursum = max(nums[i], cursum+nums[i])
		maxsum = max(cursum, maxsum)
	return maxsum

if __name__ == '__main__':
	nums = [-2,1,-3,4,-1,2,1,-5,4]
	res = maximum_subarray(nums)
	print "结果是", res
